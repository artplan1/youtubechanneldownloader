﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using YoutubeExtractor;

namespace YoutubeChannelDownloader
{
    class Program
    {
        private static string channel;
        static void Main(string[] args)
        {
            Console.WriteLine("Enter channel name:");
            channel = Console.ReadLine();
            Console.WriteLine("Fetching urls");
            List<string> links = ReadXML(channel);
            if (links.Count > 0)
            {
                foreach (string link in links)
                {
                    Console.WriteLine("Downloading {0}", link);
                    dlAudio(link);
                }
                Console.WriteLine("Download complete\r\nPress any key to exit");
                Console.ReadKey();
            }
        }

        public static List<string> ReadXML(string ch)
        {
            var urls = new List<string>();
            try
            {
                using (XmlTextReader reader = new XmlTextReader(@"https://gdata.youtube.com/feeds/api/users/" + ch + @"/uploads?max-results=50&start-index=1&prettyprint=true&fields=entry(media:group(media:player(@url)))"))
                {
                    while (reader.Read())
                    {
                        if (reader.Name == "media:player")
                        {
                            var url = reader.GetAttribute(0).Split('&')[0];
                            urls.Add(url);
                            Console.WriteLine(url);
                        }
                    }
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
            return urls;
        }

        public static void dlAudio(string link) {
            try
            {
                IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(link);

                VideoInfo video = videoInfos
                    .Where(info => info.CanExtractAudio)
                    .OrderByDescending(info => info.AudioBitrate)
                    .First();

                if (video.RequiresDecryption)
                {
                    DownloadUrlResolver.DecryptDownloadUrl(video);
                }

                System.IO.Directory.CreateDirectory(channel);

                var audioDownloader = new AudioDownloader(video, Path.Combine(channel, video.Title + video.AudioExtension));

                audioDownloader.DownloadProgressChanged += (sender, args) => Console.WriteLine(args.ProgressPercentage * 0.85);
                audioDownloader.AudioExtractionProgressChanged += (sender, args) => Console.WriteLine(85 + args.ProgressPercentage * 0.15);

                audioDownloader.Execute();
            }
            catch(Exception ex){
                Console.WriteLine(ex.Message);
            }
        }
    }
}
